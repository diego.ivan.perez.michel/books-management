import connect, {sql} from '@databases/sqlite';
import fs from 'fs';

const deleteDbIfExist = () => {
  return new Promise<void>((resolve, reject) => {
    // delete file if exists
    fs.unlink('books.db', (err) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
};

export default async () => {
  await deleteDbIfExist();
  console.log(`*** Creating Tables ***`);

  const db = connect('books.db');

  await db.query(sql`
    CREATE TABLE IF NOT EXISTS Books (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title VARCHAR NOT NULL,
      author VARCHAR NOT NULL
    );
  `);

  await db.query(sql`
    CREATE TABLE IF NOT EXISTS Pages (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      page_index INTEGER,
      content VARCHAR NOT NULL,
      book_id INTEGER
    );
  `);

  console.log(`*** Finished Creating Tables ***`);
};

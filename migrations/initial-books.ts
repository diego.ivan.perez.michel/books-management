import { Book } from '../models/books';

const initialBooks: Book[] = [
  {
    id: 1,
    title: "A Song of Ice and Fire",
    author: "George R. R. Martin",
    pages: [
      {
        pageIndex: 1,
        content: `# Species aut Iove ibi utque damnarat

        ## Succedit colubrae genitor superare ictus lurida
        
        Lorem markdownum vulnera Herculeo inserit puerpera herbae, et natis: ponit! Hanc
        et tantumque potero quem tempora rotarum micantes, stratis fugit! Dominam
        mortalia mare futuri et neque. Est Venus, Troiae Hibero montibus, mea ego aut
        natarum, et quot secundas infantia.
        
        Confusa tamen. Dederat aras relatu, greges puppim, laesit. Et negat.
        `
      },
      {
        pageIndex: 2,
        content: `## Non si signum sinistra dirusque castos

        Purpuraque ortum Tyrrhena gaudenti ad Ulixes suaque esse, sua est amnis ore
        agros illa, dederat, calido. Sint neque aequor *frequentes finxit*, rege quas
        nulla superest Salamis! Iniquis ubi abesse aliquo virginea multae delapsaque
        quidem, et et tanta sedibus tulit.`
      },
      {
        pageIndex: 3,
        content: `## Gaudet vina tam et enim qui tibi

        Lorem markdownum *vino Threicio*, molle nentes Iovis, *ubi*? Temperius Troades
        ferar.
        
        Ait sol iurgia, iamque est dedignata terrae, volucresque Polyxena. Triste adero
        sed a daulida terris possit defecta rerum Saturnia vestigia sagittifera adsum
        fraxineam, ignotos vocem aevi teloque erat. Manus nomenque aequoreo subolemque
        ad herbas quota hic octavo longa! Per illas studio erit, miserabile non pondere
        pigra amans exequialia solas enim tamen rotarum ope alebat mecum, nec.
        
        > Indicio saepe: quem Cyanes neve, vos apri, ignara significent. Iacet petebatur
        > pectore.
        
        ## Lacrimans occupat nos imagine Achaia`
      }
    ]
  },
  {
    id: 2,
    title: "Harry Potter",
    author: "J. K. Rowling",
    pages: [
      {
        pageIndex: 1,
        content: `## Gaudet vina tam et enim qui tibi

        Lorem markdownum *vino Threicio*, molle nentes Iovis, *ubi*? Temperius Troades
        ferar.
        
        Ait sol iurgia, iamque est dedignata terrae, volucresque Polyxena. Triste adero
        sed a daulida terris possit defecta rerum Saturnia vestigia sagittifera adsum
        fraxineam, ignotos vocem aevi teloque erat. Manus nomenque aequoreo subolemque
        ad herbas quota hic octavo longa! Per illas studio erit, miserabile non pondere
        pigra amans exequialia solas enim tamen rotarum ope alebat mecum, nec.
        
        > Indicio saepe: quem Cyanes neve, vos apri, ignara significent. Iacet petebatur
        > pectore.
        
        ## Lacrimans occupat nos imagine Achaia`
      },
      {
        pageIndex: 2,
        content: `# Species aut Iove ibi utque damnarat

        ## Succedit colubrae genitor superare ictus lurida
        
        Lorem markdownum vulnera Herculeo inserit puerpera herbae, et natis: ponit! Hanc
        et tantumque potero quem tempora rotarum micantes, stratis fugit! Dominam
        mortalia mare futuri et neque. Est Venus, Troiae Hibero montibus, mea ego aut
        natarum, et quot secundas infantia.
        
        Confusa tamen. Dederat aras relatu, greges puppim, laesit. Et negat.
        `
      },
      {
        pageIndex: 3,
        content: `## Non si signum sinistra dirusque castos

        Purpuraque ortum Tyrrhena gaudenti ad Ulixes suaque esse, sua est amnis ore
        agros illa, dederat, calido. Sint neque aequor *frequentes finxit*, rege quas
        nulla superest Salamis! Iniquis ubi abesse aliquo virginea multae delapsaque
        quidem, et et tanta sedibus tulit.`
      }
    ]
  },
  {
    id: 3,
    title: "The Hunger Games",
    author: "Suzanne Collins",
    pages: [
      {
        pageIndex: 1,
        content: `## Gaudet vina tam et enim qui tibi

        Lorem markdownum *vino Threicio*, molle nentes Iovis, *ubi*? Temperius Troades
        ferar.
        
        Ait sol iurgia, iamque est dedignata terrae, volucresque Polyxena. Triste adero
        sed a daulida terris possit defecta rerum Saturnia vestigia sagittifera adsum
        fraxineam, ignotos vocem aevi teloque erat. Manus nomenque aequoreo subolemque
        ad herbas quota hic octavo longa! Per illas studio erit, miserabile non pondere
        pigra amans exequialia solas enim tamen rotarum ope alebat mecum, nec.
        
        > Indicio saepe: quem Cyanes neve, vos apri, ignara significent. Iacet petebatur
        > pectore.
        
        ## Lacrimans occupat nos imagine Achaia`
      },
      {
        pageIndex: 2,
        content: `## Non si signum sinistra dirusque castos

        Purpuraque ortum Tyrrhena gaudenti ad Ulixes suaque esse, sua est amnis ore
        agros illa, dederat, calido. Sint neque aequor *frequentes finxit*, rege quas
        nulla superest Salamis! Iniquis ubi abesse aliquo virginea multae delapsaque
        quidem, et et tanta sedibus tulit.`
      },
      {
        pageIndex: 3,
        content: `# Species aut Iove ibi utque damnarat

        ## Succedit colubrae genitor superare ictus lurida
        
        Lorem markdownum vulnera Herculeo inserit puerpera herbae, et natis: ponit! Hanc
        et tantumque potero quem tempora rotarum micantes, stratis fugit! Dominam
        mortalia mare futuri et neque. Est Venus, Troiae Hibero montibus, mea ego aut
        natarum, et quot secundas infantia.
        
        Confusa tamen. Dederat aras relatu, greges puppim, laesit. Et negat.
        `
      }
    ]
  }
];

export default initialBooks;
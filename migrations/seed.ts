import connect, {DatabaseConnection, sql} from '@databases/sqlite';
import initialBooks from './initial-books';
import {Book, Page} from '../models/books';

const insertBook = async (db: DatabaseConnection, book: Book) => {
  await db.query(sql`
  INSERT INTO Books (id, title, author) VALUES (${book.id}, ${book.title}, ${book.author});
`);
};

const insertPage = async (
  db: DatabaseConnection,
  page: Page,
  bookId?: number
) => {
  await db.query(sql`
  INSERT INTO Pages (page_index, content, book_id) VALUES (${page.pageIndex}, ${page.content}, ${bookId});
`);
};

export default async () => {
  console.log(`*** Running Seeder ***`);

  const db = connect('books.db');

  for (let book of initialBooks) {
    await insertBook(db, book);

    for (let page of book.pages) {
      await insertPage(db, page, book.id);
    }
  }

  console.log(`*** Finished Running Seeder ***`);
};

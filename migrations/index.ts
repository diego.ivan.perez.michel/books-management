import createTables from './create-tables';
import seed from './seed';

const run = async () => {
  console.log(`*** Running Migrations ***`);
  await createTables();
  await seed();
  console.log(`*** Finished Running Migrations ***`);
};

run();

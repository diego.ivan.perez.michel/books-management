import http from 'http';
import {pathToRegexp} from 'path-to-regexp';
import BooksController from './controllers/books-controller';
import {PageFormat} from './models/books';
import routes from './routes';
import BooksService from './services/books-service';

const server = http.createServer(async (req, res) => {
  if (!req.url) {
    res.writeHead(404, {'Content-Type': 'application/json'});
    res.end(JSON.stringify({message: 'Url not provided'}));
    return;
  }

  // /books GET
  if (
    req.url.match(pathToRegexp(routes.GET_ALL_BOOKS.path)) &&
    req.method === routes.GET_ALL_BOOKS.method
  ) {
    const books = await new BooksController(new BooksService()).getBooks();
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(books));
  }
  // /books/:id GET
  else if (
    req.url.match(pathToRegexp(routes.GET_ONE_BOOK.path)) &&
    req.method === routes.GET_ONE_BOOK.method
  ) {
    try {
      const id = Number.parseInt(req.url.split('/')[2]);
      const book = await new BooksController(new BooksService()).getBook(id);

      if (!book) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({message: 'Content Not found'}));
        return;
      }

      res.writeHead(200, {'Content-Type': 'application/json'});
      res.end(JSON.stringify(book));
    } catch (error) {
      res.writeHead(404, {'Content-Type': 'application/json'});
      res.end(JSON.stringify({message: error}));
    }
  }
  // /books/:bookId/page/:pageId/:format GET
  else if (
    (req.url.match(pathToRegexp(routes.GET_BOOK_PAGE.path)) ||
      req.url.match(pathToRegexp(routes.GET_BOOK_PAGE.defaultPath))) &&
    req.method === routes.GET_BOOK_PAGE.method
  ) {
    try {
      const bookId = Number.parseInt(req.url.split('/')[2]);
      const pageId = Number.parseInt(req.url.split('/')[4]);
      const format = req.url.split('/')[5];

      const pageContent = await new BooksController(
        new BooksService()
      ).getBookPage(
        bookId,
        pageId,
        format == 'html' ? PageFormat.HTML : PageFormat.PLAIN_TEXT
      );

      if (!pageContent) {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({message: 'Content Not found'}));
        return;
      }

      res.writeHead(200, {
        'Content-Type':
          format == 'html'
            ? 'text/html; charset=UTF-8'
            : 'text/plain; charset=UTF-8',
      });
      res.end(pageContent);
    } catch (error) {
      res.writeHead(404, {'Content-Type': 'application/json'});
      res.end(JSON.stringify({message: error}));
    }
  }
  // If no route present
  else {
    res.writeHead(404, {'Content-Type': 'application/json'});
    res.end(JSON.stringify({message: 'Resource not found'}));
  }
});

export default server;

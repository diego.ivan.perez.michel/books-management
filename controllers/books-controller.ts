import {markdownToTxt} from 'markdown-to-txt';
import showdown from 'showdown';
import {Book, PageFormat} from '../models/books';
import IBookService from '../services/interfaces/i-book-service';

class BooksController {
  constructor(protected booksService: IBookService) {}

  async getBooks(): Promise<Book[]> {
    return this.booksService.getBooks();
  }

  async getBook(id: number): Promise<Book | null> {
    return this.booksService.getBookById(id);
  }

  async getBookPage(
    bookId: number,
    pageIndex: number,
    format: PageFormat
  ): Promise<string | null> {
    const page = await this.booksService.getBookPage(bookId, pageIndex);

    if (!page) {
      return null;
    }

    let content = '';

    switch (format) {
      case PageFormat.PLAIN_TEXT:
        content = markdownToTxt(page.content);
        break;
      case PageFormat.HTML:
        const converter = new showdown.Converter();
        content = converter.makeHtml(page.content);
        break;
    }

    return content;
  }
}

export default BooksController;

export interface Book {
  id?: number;
  title: string;
  pages: Page[];
  author: string;
  pagesCount?: number;
}

export interface Page {
  id?: number;
  pageIndex: number;
  content: string;
}

export enum PageFormat {
  PLAIN_TEXT,
  HTML
}
# Books Management

REST Api for Books Management

# Getting Started

> To get started setting up this app follow the next steps

First to get the dependencies run

```bash
  npm install
```

For generating the database and seeding initial data we have to run migration script

```bash
  npm run migrate
```

Finally we can run the app by executing

```bash
  npm run start
```

### That's it! you're all setup.

## Start using it

This API will run by default at port 5000.

We have the next available endponts:

### Routes

* **/books**
  * Get the list of available books. Example GET /books
* **/books/:id**
  * Get one single book from our database. Example: GET /books/1
* **/books/:bookId/page/:pageId/:format**
  * Get one page in the desire format from the specified book. Default: plain text, available format: **text**, **html**. Example: GET /books/1/page/3/html

export default {
  GET_ALL_BOOKS: {
    path: '/books',
    method: 'GET'
  },
  GET_ONE_BOOK: {
    path: '/books/:id',
    method: 'GET'
  },
  GET_BOOK_PAGE: {
    defaultPath: '/books/:bookId/page/:pageId',
    path: '/books/:bookId/page/:pageId/:format',
    method: 'GET'
  }
};
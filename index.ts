import server from "./server";

const PORT = process.env.PORT || 5000;

const main = {
  start: () => {
    console.log(`*** Server Listing at port ${PORT} ***`);
    server.listen(PORT);
  },
};

main.start();

import connect, {sql} from '@databases/sqlite';
import {Book, Page} from '../models/books';
import IBookService from './interfaces/i-book-service';

class BooksService implements IBookService {
  async getBooks(): Promise<Book[]> {
    const db = connect('books.db');
    const results = await db.query(sql`
      SELECT Books.id, title, author, COUNT(Pages.book_id) AS pagesCount FROM Books JOIN Pages ON Books.id = Pages.book_id GROUP BY Books.id;
    `);

    return results;
  }

  async getBookById(id: number): Promise<Book | null> {
    const db = connect('books.db');
    const results = await db.query(sql`
      SELECT Books.id, title, author, COUNT(Pages.book_id) AS pagesCount FROM Books JOIN Pages ON Books.id = Pages.book_id WHERE Books.id = ${id};
    `);

    if (results.length == 0) {
      return null;
    }

    return results[0];
  }

  async getBookPage(bookId: number, pageIndex: number): Promise<Page | null> {
    const db = connect('books.db');
    const results = await db.query(sql`
      SELECT content FROM Pages WHERE book_id = ${bookId} AND page_index = ${pageIndex};
    `);

    if (results.length == 0) {
      return null;
    }

    return results[0];
  }
}

export default BooksService;

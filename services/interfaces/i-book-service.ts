import {Book, Page, PageFormat} from '../../models/books';

interface IBookService {
  getBooks(): Promise<Book[]>;
  getBookById(id: number): Promise<Book | null>;
  getBookPage(bookId: number, pageIndex: number): Promise<Page|null>;
}

export default IBookService;
